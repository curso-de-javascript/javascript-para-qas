var userName = 'Ericton Felicidade'

document.getElementById('user-name').innerHTML = userName;

// JAVACRIPT

var nome = 'Mestre Yoda'
var idade = 100
var jedi = true

// variaveis //
// console.log('Nome', typeof nome)
// console.log('Idade', typeof idade)
// console.log('É um Jedi', typeof jedi)



// var n1 = 30
// var n2 = 7

// var total = n1 - n2

// console.log(total)

// + para somar
// - para subtrair
// * para multiplicar
// / para dividir

// var total = n1 / n2

// console.log(total)

// Operadores matemáticos //

// var v1 = 5
// var v2 = "5"

// var resutado = v1 === v2

// console.log(resutado)


// Funções

// function soma(n1, n2){
//     console.log(n1 + n2)
// }

// soma(5, 100)

// function boasVindas(nome) {
//     alert(nome + ', seja bem vindo(a)')
// }

// boasVindas('Ericton')

// function soma(n1, n2) {
//     return n1 + n2
// }

// var resutado = soma(5, 5)
// console.log(resutado)

// Controle de fluxos

// Sendo um cliente correntista do banco
// Posso sacar dinheiro em caixas eletrônicos
// Para poder comprar em lugares que não aceitam o cartão de débito ou crédito

// var saldo = 1000;

// function saque(valor) {

//     if (valor > saldo) {
//         console.log('Valor do saque superior ao saldo')
//     } else if (valor > 700) {
//         console.log('Valor do saque é superior ao máximo permitido por operação')
//     } else {
//         saldo = saldo - valor
//     }
// }
// saque(701)
// console.log(saldo)

// Cenário 1: Saque com sucesso
// Dado que meu saldo é de 1000 reais
// Quaqndo faço um saque de 500 reais
// Então o valor do saque deve ser deduzir do meu saldo

// Cenário 2: Saque com valor superior ao saldo
// Dado que meu saldo é de 1000 reais
// Quando faço um saque de 1001 reais
// Então não deve deduzir do meu saldo
// E deve mostrar uma mensagem de alerta informando que o valor é superior ao saldo

// Cenário 3: Saque com valor máximo
// Dado que meu saldo é de 1000 reais
// E o valor máximo por operação é de 700
// Quando faço um saque no valor de 701 reais
// E deve mostrar uma mensagem de alerta informando que o valor é superior ao máximo permitido por operação

// Arrays //

// var gaveteiro = ['Meias', 'Gravatas', 'Documentos', 'Salgadinhos']

// console.log([3])

// var personagens = ['YODA', 'LUKE', 'LEIA', 'DART VADER']

// personagens.push('C3pO')
// personagens.push('R@D@')

// // personagens.pop()

// personagens = personagens.filter(function(p) {
//     return p !== 'DART VADER'
// })

// personagens = personagens.filter(function(p) {
//     return p === 'YODA'
// })

// console.log(personagens)

// estrutura de repetição
// var personagens = ['YODA', 'LUKE', 'LEIA', 'DART VADER']

// personagens.forEach(function(p) {
//     console.log(p)
// })


// for (var i in personagens) {
//     console.log(personagens[i])
// }

// for (var i = 0; i <= 10; i++) {
//     console.log(i)
// }


// OBJETOS //

// const nome = 'Dart Vader'
// console.log(nome)

// nome = 'Mestre Yoda'
// console.log(nome)
